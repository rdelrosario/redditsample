﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RedditSample.Services;

namespace RedditSample.Droid.Services
{
    public class SaveImageService : ISaveImageService
    {
        public Task<bool> SaveFromUrl(string url)
        {
            bool retVal = false;
            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    Bitmap imageBitmap = null;
                    using (var webClient = new WebClient())
                    {
                        var imageBytes = webClient.DownloadData(url);
                        if (imageBytes != null && imageBytes.Length > 0)
                        {
                            string name = "REDDIT-" + System.DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg";
                            var file = new Java.IO.File(GetDirectoryForPictures("REDDIT"), name);

                            System.IO.File.WriteAllBytes(file.Path, imageBytes);

                            try
                            {
                                var mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                                Android.Net.Uri contentUri = Android.Net.Uri.FromFile(new Java.IO.File(file.Path));
                                mediaScanIntent.SetData(contentUri);
                                Xamarin.Forms.Forms.Context.SendBroadcast(mediaScanIntent);
                                retVal = true;
                            }
                            catch (Exception ex)
                            {
                               
                                System.Diagnostics.Debug.WriteLine(ex.Message);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }
            return Task.FromResult(retVal);
        }

        Java.IO.File GetDirectoryForPictures(string collectionName)
        {
            var fileDir = new Java.IO.File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), collectionName);
            if (!fileDir.Exists())
            {
                fileDir.Mkdirs();
            }

            return fileDir;

        }
       
    }
}