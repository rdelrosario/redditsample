﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using RedditSample.Managers;
using RedditSample.Services;
using RedditSample.ViewModels;
using RedditSample.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace RedditSample
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }
        static IApiService<IRedditApi> redditApi = new ApiService<IRedditApi>(Config.ApiUrl);

        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.NavigateAsync(new System.Uri($"/{NavigationConstants.MasterPage}/{NavigationConstants.NavigationPage}/{NavigationConstants.PostDetail}", System.UriKind.Absolute));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<CustomNavigationPage>();
            containerRegistry.RegisterForNavigation<CustomMasterDetailPage, CustomMasterDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<PostDetailPage, PostDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<ImagePreviewPage, ImagePreviewPageViewModel>();


            containerRegistry.RegisterInstance<IConnectivity>(CrossConnectivity.Current);
            containerRegistry.RegisterInstance<IApiService<IRedditApi>>(redditApi);
            containerRegistry.RegisterSingleton<IApiManager, ApiManager>();
        }
    }
}
