﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedditSample
{
    public static class NavigationConstants
    {
        public const string NavigationPage = "CustomNavigationPage";
        public const string PostDetail = "PostDetailPage";
        public const string MasterPage = "CustomMasterDetailPage";
        public const string ImagePreview = "ImagePreviewPage";
    }

    public static class NavParams
    {
        public const string PostDetail = "PostDetail";
        public const string Image = "PostImage";
    }

}
