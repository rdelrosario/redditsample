﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedditSample.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading.Tasks;

    namespace RedditSample.ViewModels
    {
        public class BaseViewModel : INotifyPropertyChanged
        {
            public bool IsBusy { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;

            public async Task RunSafe(Task task)
            {
                try
                {
                    if (IsBusy) return;

                    IsBusy = true;


                    await task;
                }
                catch (Exception e)
                {
                    IsBusy = false;
                    Debug.WriteLine(e.ToString());
                    await App.Current.MainPage.DisplayAlert("Eror", "Check your internet connection", "Ok");

                }
                finally
                {
                    IsBusy = false;
                }
            }

        }
    }
}
