﻿using Refit;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedditSample.Services
{
    [Headers("Content-Type: application/json")]
    public interface IRedditApi
    {
        [Get("/new.json?sort=top&raw_json=1&limit={limit}&after={nextPage}")]
        Task<HttpResponseMessage> GetPosts(int limit, string nextPage, CancellationToken cancellationToken);
    }
}
