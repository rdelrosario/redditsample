﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedditSample.Services
{
    public interface ISaveImageService
    {
        Task<bool> SaveFromUrl(string url);
    }
}
