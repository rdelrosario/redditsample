﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RedditSample.Managers
{
    public interface IApiManager
    {
        Task<HttpResponseMessage> GetPosts(string nextPage, int limit = 20);
    }
}
