﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace RedditSample.Views
{
    public class CustomNavigationPage : NavigationPage
    {
        public CustomNavigationPage()
        {
           BarBackgroundColor = Color.Black;
           BarTextColor = Color.White;
        }
    }
}
