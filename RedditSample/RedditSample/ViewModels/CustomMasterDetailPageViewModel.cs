﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using RedditSample.Managers;
using RedditSample.Models;
using RedditSample.ViewModels.RedditSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedditSample.ViewModels
{
    public class CustomMasterDetailPageViewModel : BaseViewModel
    {
        INavigationService _navigationService;
        IApiManager _apiManager;
        IPageDialogService _pageDialogService;
        public DelegateCommand<PostItem> OnNavigateCommand { get; set; }
        public DelegateCommand RefreshListCommand { get; set; }
        public DelegateCommand OnDismissAllCommand { get; set; }
        public DelegateCommand<PostItem> DismissItemCommand { get; set; }
        public DelegateCommand LoadMoreCommand { get; set; }
        public ObservableCollection<PostItem> PostItems { get; set; }
        string _nextPageId = string.Empty;
        string _actualPageId = string.Empty;
        public bool ListHasData { get; set; }
        public CustomMasterDetailPageViewModel(INavigationService navigationService, IApiManager apiManager, IPageDialogService pageDialogService)
        {
            _navigationService = navigationService;
            _apiManager = apiManager;
            _pageDialogService = pageDialogService;
            OnNavigateCommand = new DelegateCommand<PostItem>(async (param) => await NavigateAync(param));
            RefreshListCommand = new DelegateCommand(async () => await OnRefreshList());
            LoadMoreCommand = new DelegateCommand(async () => await LoadMore());
            OnDismissAllCommand = new DelegateCommand(Dismiss);
            DismissItemCommand = new DelegateCommand<PostItem>(DismissItem);
            RefreshListCommand.Execute();
        }

        async Task NavigateAync(PostItem item)
        {
            item.Data.IsNotViewed = false;
            var param = new NavigationParameters() { { NavParams.PostDetail, item } };
            await _navigationService.NavigateAsync(new Uri($"{NavigationConstants.NavigationPage}/{NavigationConstants.PostDetail}", UriKind.Relative), param);
        }
        void DismissItem(PostItem param)
        {
            PostItems.Remove(param);
            CheckData();
        }
        async void Dismiss()
        {
            _nextPageId = _actualPageId = string.Empty;
            PostItems?.Clear();
            CheckData();
        }

        async Task OnRefreshList()
        {
            await RunSafe(GetData());
        }

        async Task LoadMore()
        {
            if (!string.IsNullOrEmpty(_nextPageId))
            {
                _actualPageId = _nextPageId;
                await RunSafe(GetData(true));
            }
        }

        async Task GetData(bool loadMore = false)
        {

            IsBusy = true;
            var transactionReceiptsResponse = await _apiManager.GetPosts(_actualPageId);

            if (transactionReceiptsResponse.IsSuccessStatusCode)
            {
                var responseContent = await transactionReceiptsResponse.Content.ReadAsStringAsync();
                var result = await Task.Run(() => JsonConvert.DeserializeObject<RootPostItem>(responseContent));
                _nextPageId = result?.Data.After;

                if (result.Data?.Children != null && result.Data?.Children.Count > 0)
                {
                    if (loadMore)
                    {
                        foreach (var item in result?.Data.Children)
                        {
                            PostItems.Add(item);
                        }
                    }
                    else
                    {
                        PostItems = new ObservableCollection<PostItem>(result.Data.Children);
                        await NavigateAync(PostItems.First());
                    }
                }
            }
            else
            {
                IsBusy = false;
                await _pageDialogService.DisplayAlertAsync(Config.AppName, "Unable to get data", "Ok");
            }
            CheckData();
            IsBusy = false;
        }

        void CheckData()
        {
            ListHasData = (PostItems != null && PostItems.Count > 0);
        }
    }
}
