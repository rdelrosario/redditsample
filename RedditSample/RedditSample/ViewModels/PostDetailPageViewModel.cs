﻿using Prism.Commands;
using Prism.Navigation;
using RedditSample.Models;
using RedditSample.ViewModels.RedditSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedditSample.ViewModels
{
    public class PostDetailPageViewModel : BaseViewModel, INavigatingAware
    {
        INavigationService _navigationService;
        public PostItem PostItem { get; set; }

        public DelegateCommand OnShowImageCommand { get; set; }
        public PostDetailPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            OnShowImageCommand = new DelegateCommand(async () => await ShowImageAync());
        }

        async Task ShowImageAync()
        {
            var param = new NavigationParameters() { { NavParams.Image, PostItem.Data.MainImage } };
            await _navigationService.NavigateAsync(new Uri($"{NavigationConstants.ImagePreview}", UriKind.Relative),param);
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey(NavParams.PostDetail))
            {
                PostItem = parameters[NavParams.PostDetail] as PostItem;
            }
        }
    }
}
