﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using RedditSample.Services;
using RedditSample.ViewModels.RedditSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedditSample.ViewModels
{
    public class ImagePreviewPageViewModel : BaseViewModel, INavigatingAware
    {
        public DelegateCommand OnSaveCommand { get; set; }
        ISaveImageService _saveImageService;
        IPageDialogService _pageDialogService;
        public string ImageUrl { get; set; }

        public ImagePreviewPageViewModel(ISaveImageService saveImageService, IPageDialogService pageDialogService)
        {
            _pageDialogService = pageDialogService;
            _saveImageService = saveImageService;
            OnSaveCommand = new DelegateCommand(async()=> await OnSave());
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey(NavParams.Image))
            {
                ImageUrl = $"{parameters[NavParams.Image]}";
            }
        }

        async Task OnSave()
        {
           bool result = await _saveImageService.SaveFromUrl(ImageUrl);
            if(result)
            {
                await _pageDialogService.DisplayAlertAsync(Config.AppName, "Image saved succesfully","Ok");
            }
            else
            {
                await _pageDialogService.DisplayAlertAsync(Config.AppName, "Couldn't save image", "Ok");
            }
        }
    }
}
