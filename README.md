# RedditSample

Total time spent: 5 hours.

Features missing:

- App state-preservation/restoration
- Animations

**Note: Didn't implement the above since feel this were the nice to have features. Had to prioritize the other functionality that was more important based on the limited timeframe.**

For dismiss options used Context Actions:

- On Android you dimiss by doing long press
- On iOS you can dismiss by swiping and clicking on dismiss option.